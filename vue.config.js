const CompressionWebpackPlugin = require('compression-webpack-plugin')
const productionGzipExtensions = ['js', 'css']
const path = require('path')
const resolve = (dir) => path.join(__dirname, dir)

module.exports = {
  // 部署应用包时的基本URL。
  publicPath: process.env.NODE_ENV === 'production' ? './' : '/',

  // 生成环境构建文件的目录。构建时传入 --no-clean 可关闭该行为
  outputDir: 'sams-web',

  // 放置生成的静态资源(js, css, img, fonts)d的(相对于 outputDir 的)目录
  assetsDir: '',

  // 指定生成的 index.html 的输出路径 (相对于 outputDir ).也可以是一个绝对路径
  indexPath: 'index.html',

  // 生成的静态资源在它们的文件名中包含了hash以便更好的控制缓存。
  // 如果你无法使用Vue CLI生成的 index HTML,你可以通过将这个选项设为 false 来关闭文件名哈希
  filenameHashing: true,

  // 页面配置
  pages: {
    index: {
      // 页面入口
      entry: 'src/main.js',
      // 模板来源
      template: 'public/index.html',
      filename: 'index.html',
      // 当使用 title 选项时，
      // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
      title: '安全自主监控系统',
      // 在这个页面中包含的块，默认情况下会包含提取出来的通用 chunk 和 vendor chunk
      chunks: ['chunk-vendors', 'chunk-common', 'index']
    }
  },

  // 是否使用包含运行时编译器的Vue构建版本
  // 设置为 true 后你就可以在 Vue 组件中使用 template 选项了，但是这会让你的应用额外增加 10kb 左右
  runtimeCompiler: true,

  // 默认情况下 babel-loader 会忽略所有 node_modules 中的文件。如果你想要通过 Babel 显式转译一个依赖，可以在这个选项中列出来
  transpileDependencies: [],

  // 如果你不需要生产环境的 source map, 可以将其设置为 false 以加速生产环境构建
  productionSourceMap: true,

  // 设置生产的 HTML 中 <link rel="stylesheet"> 和 <script> 标签的 crossorigin 属性
  // 需要注意的是该选项仅影响由 html-webpack-plugin 在构建时注入的标签 - 直接写在模板 (public/index.html) 中的标签不受影响
  crossorigin: undefined,

  // 在生成的 HTML 中的 <link rel="stylesheet"> 和 <script> 标签上启用 Subresource Integrity (SRI)。
  // 如果你构建后的文件是部署在 CDN 上的，启用该选项可以提供额外的安全性。
  // 需要注意的是该选项仅影响由 html-webpack-plugin 在构建时注入的标签 - 直接写在模版 (public/index.html) 中的标签不受影响。
  // 另外，当启用 SRI 时，preload resource hints 会被禁用，因为 Chrome 的一个 bug 会导致文件被下载两次。
  integrity: false,

  lintOnSave: process.env.NODE_ENV !== 'production',

  // 值如果是一个对象，则会通过 webpack-merge 合并到最终的配置中
  // 值如果是一个函数，则会接收被解析的配置作为参数。
  // 该函数及可以修改配置并不返回任何东西，也可以返回一个被克隆或合并过的配置版本。
  configureWebpack: config => {
    if (process.env.NODE_ENV === 'production') {
      // 为生产环境修改配置...
      config.plugins.push(new CompressionWebpackPlugin({
        algorithm: 'gzip',
        test: new RegExp(`\\.(${productionGzipExtensions.join('|')})$`),
        threshold: 10240,
        minRatio: 0.8
      }))
    } else {
      // 为开发环境修改配置...
      config.devtool = 'source-map'
    }
  },

  // 链式操作 Vue CLI内部的 webpack 配置是通过 webpack-chain 维护的。
  // 这个库提供了一个 webpack 原始配置的上层抽象，使其可以定义具名的 loader 规则和具名插件，并有机会在后期进入这些规则并对它们的选项进行修改
  chainWebpack: config => {
    config.module
      .rule('vue')
      .use('vue-loader')
      .loader('vue-loader')
      .tap(options => {
        // 修改它的选项
        return options
      })
    config.resolve.alias
      .set('@', resolve('src'))
      .set('~', resolve('src/assets'))
  },

  // 开发环境配置
  devServer: {
    // 在设置让浏览器 overlay 同时显示警告和错误
    overlay: {
      warnings: false,
      errors: true
    },
    // 自动打开浏览器
    open: true,
    // 设置为0.0.0.0则所有的地址均能访问
    host: '0.0.0.0',
    // 端口
    port: 8000,
    https: false,
    hotOnly: false,
    proxy: {
      '/': {
        target: 'http://192.168.3.174:8080',
        ws: true,
        changeOrigin: true,
        pathRewrite: {
          '^/': ''
        }
      },
    }
  }
}
