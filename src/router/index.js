import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '../views/layout/layout.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/manage'
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/login/login.vue')
  },
  {
    path: '/',
    component: Layout,
    children: [
      {
        path: 'manage',
        name: 'manage',
        component: () => import('../views/operationManagement/manage.vue')
      },
      {
        path: 'person',
        name: 'person',
        component: () => import('../views/person/person.vue')
      },
      {
        path: 'ship',
        name: 'ship',
        component: () => import('../views/ship/ship.vue')
      }
    ]
  }
]

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
